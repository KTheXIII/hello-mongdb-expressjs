# Hello MongoDB with Express

Learning how to use MongoDB with Express and Docker. A full stack application.

## Requirements

### Development

You need [nodejs](https://nodejs.org/en/) and [docker](https://www.docker.com) installed.

### Production

You only need [docker](https://www.docker.com) installed.
