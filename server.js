const http = require('http')
const consola = require('consola')
const app = require('./backend/app')

const PORT = process.env.PORT | 5000

const server = http.createServer(app)

server.listen(PORT, () => {
  consola.ready({
    message: `Server listening on http://localhost:${PORT}`,
    badge: true,
  })
})
