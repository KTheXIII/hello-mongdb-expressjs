const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

// Log configs
const stats = {
  children: false,
  colors: true,
  modules: false,
  timings: true,
}

module.exports = {
  entry: './frontend/main.js',
  stats,
  output: {
    path: path.join(__dirname, '../', 'dist'),
    filename: 'main.[hash].js',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules|backend/,
        use: [
          {
            loader: 'babel-loader',
          },
        ],
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './frontend/index.html',
    }),
  ],
}
