const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const consola = require('consola')

const items = require('./routes/api/items')

const app = express()

// Body Parser Middleware
app.use(bodyParser.json())

// DB config
const db = require('../configs/keys')

// Connect to MongoDB
mongoose
  .connect(db.mongoURI, {
    auth: {
      authSource: 'admin',
    },
    user: db.username,
    pass: db.password,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    consola.success('Database connected!')
  })
  .catch((err) => {
    consola.error(err)
  })

// Use Routes
app.use('/api/items', items)

module.exports = app
